# Base our image on NodeJS first
FROM node:22.12.0
LABEL maintainer="oss@xtrm.me"

# Then download deno
COPY --from=denoland/deno:bin-2.1.4 /deno /deno

