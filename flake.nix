{
  description = "dev-shell for x's website";

  inputs = {
    systems.url = "github:nix-systems/x86_64-linux";
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs =
    {
      self,
      nixpkgs,
      systems,
    }:
    let
      forEachSystem =
        f:
        (nixpkgs.lib.genAttrs (import systems) (
          system:
          f (
            import nixpkgs {
              inherit system;
            }
          )
        ));

      provideNativeInputs = pkgs: with pkgs; [
        deno
      ];
    in
    {
      formatter = forEachSystem (pkgs: pkgs.nixfmt-rfc-style);

      devShell = forEachSystem (pkgs: pkgs.mkShell {
        nativeBuildInputs = provideNativeInputs pkgs;
      });
    };
}

# vim: set ft=nix ts=2 sw=2 et:
