# xtrm.me [![status-badge](https://ci.codeberg.org/api/badges/13964/status.svg)](https://ci.codeberg.org/repos/13964)

my new website, built on top of [Lume](https://lume.land), a static site generator for [deno](https://deno.land).

## build & serve

Building: `deno task build`

Serve locally: `deno task serve`

Serve via Cloudflare's Wrangler CLI: see [.woodpecker.yaml](./.woodpecker.yaml)

## license

This project is licensed under the [ISC license](./LICENSE).
