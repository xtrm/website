---
title: homepage
description: 'hacking away at random and useless stuff I find cool :3'
extra_head: '<link rel="me" href="https://blahaj.zone/@xtrm" title="My fediverse account">'
---
# welcome to my space :wave:, i'm **X**. 

I'm a **software engineer**, a *student*, and a ~~complete dumbass~~ on the internet.

I've been doing this whole **"programming"** thing for half of my life now, and i'd like to think i'm getting good at it.

If you're wondering what I'm doing :question:, I'm mostly doing some [school projects](https://codeberg.org/27), working on a [modding framework](https://codeberg.org/LumosMC), and doing programming all around.

:sparkles: you can go ahead and checkout my cool & infrequent [blog posts](/pages/posts.vto), get more information about me [here](/pages/about.vto) :information_source:, or check me out [somewhere else](/pages/links.vto) on the :globe_with_meridians: webbed world. ***See ya' around~***
